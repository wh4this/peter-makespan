class Instance {
    // Constructeur d'une instance (avec pour paramètre un nombre de machine et un tableau de tâches)
    constructor(nbMachines, taches) {
        this.nbTaches = taches.length;
        this.nbMachines = nbMachines;
        this.taches = taches;

        // Création des machines
        this.machines = [];
        for (let i = 0; i < nbMachines; i++) {
            this.machines[i] = new Machine();
        }

        // Calcul de la borne inférieure maximum
        this.borneInferieurMaximum = Math.max.apply(Math, this.taches.map(function(o){return o.duree;}));

        // Calcul de la borne inférieure moyenne
        var somme = 0;
        for (let i = 0; i < this.taches.length; i++) {
            somme += this.taches[i].duree;
        }
        
        this.borneInferieurMoyenne = somme/this.taches.length;
    }

     // Méthode qui permet trier les tâches dans l'ordre croissant
    comparerCroissant(a,b) {
        return a.duree - b.duree;
    }

    // Méthode qui permet trier les tâches dans l'ordre décroissant
    comparerDecroissant(a,b) { 
        return b.duree - a.duree;
    }

    // Méthode qui permet de retourner la première machine disponible
    premiereMachineDisponible() {
        var machineDisponible = this.machines[0];
        for (let i = 0; i < this.machines.length; i++) {
            if (this.machines[i].dureeTotal < machineDisponible.dureeTotal) {
                machineDisponible = this.machines[i];
            }
        }
        return machineDisponible;
    }

    // Méthode qui permet de remettre les machines à zéro (supprimer les tâches des machines)
    resetMachines() {
        for (let i = 0; i < this.machines.length; i++) {
            this.machines[i].reset();
        }
    }
}