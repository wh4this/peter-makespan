
function afficherDetail(instance, type) {
  document.getElementById('detail').innerHTML += "<br><strong>"+ type + "</strong><br><br>";
  for (let i = 0; i < instance.machines.length; i++) {
    var iDiv = document.createElement('div'+i);
    iDiv.id = 'block'+i;
    iDiv.className = 'machine';
    var nbr = i+1;
    iDiv.innerHTML += "<span id=\"nomMachine\">M"+nbr+"</span>"
    var bgColor = 1;
    var duree = 0;
    for (let k = 0; k < instance.machines[i].listeTaches.length; k++) {
      if (bgColor%2 == 0) {
        iDiv.innerHTML += "<div style=\"width:"+ instance.machines[i].listeTaches[k].duree*18+"px; background-color:#43cea2\" class=\"tache\">"
        + instance.machines[i].listeTaches[k].duree + "</div>";
      } else {
        iDiv.innerHTML += "<div style=\"width:"+ instance.machines[i].listeTaches[k].duree*18+"px; color:white; background-color:#185a9d\" class=\"tache\">"
        + instance.machines[i].listeTaches[k].duree + "</div>";
      }
      bgColor = bgColor + 1;
      duree = duree + instance.machines[i].listeTaches[k].duree;
    }
    iDiv.innerHTML += "<div class=\"tache\" id=\"dureeTache\">"+duree+"</div>";
    detail.appendChild(iDiv);
  }
}

function resetResultat() {
  document.getElementById('resultat').innerHTML = "";
}

function resetDetail() {
  document.getElementById('detail').innerHTML = "";
}

function afficherResultat(instance) {

  document.getElementById('resultat').innerHTML += "<br>Borne inférieur Maximum : " + instance.borneInferieurMaximum;
  document.getElementById('resultat').innerHTML += "<br>Borne inférieur Moyenne : " + instance.borneInferieurMoyenne;
}

function afficherResultatLSA(res) {
  document.getElementById('resultat').innerHTML += "<br>Résultat LSA : " + res;
}

function afficherResultatLPT(res) {
  document.getElementById('resultat').innerHTML += "<br>Résultat LPT : " + res;
}

function afficherResultatMyAlgo(res) {
  document.getElementById('resultat').innerHTML += "<br>Résultat MyAlgo : " + res +"<br><br>";
}
