// Fonction qui retourne une instance en fonction
// d'une chaine de caractère de la forme "m : n : d1 : d2 ... dn" nommé contenu
function lireContenu(contenu) {
    var taches = [];

    // génère un tableau de valeur en fonction de la chaine de caractère contenu
    var tab = contenu.split(":").map(function(item) {
        return parseInt(item, 10);
    });
    // Instancie et place les tâches dans un tableau
    for (let i = 0; i < tab.slice(2).length; i++) {
        taches.push(new Tache(tab.slice(2)[i]))
    }
    // Instanciation d'une instance :-)
    var i = new Instance(tab[0], taches);
    return i;
}
