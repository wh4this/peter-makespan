function validerType2(chaine) {
  var regex = /((\d+):(\d+):(\d+))(:(\d+))*$/g; // format : "integer:integer:integer(:integer)*"
  return regex.test(chaine);
}

function validerType1(chaine) {
  var regex = /((\d+):(\d+):(\d+))(:(\d+))*/g; // format : "integer:integer:integer(:integer)*"
  return regex.test(chaine);
}
