// Fonction qui calcul et affiche les résultats d'une instances
function resultat(i) {
    // Supression des anciens résultats (sur l'interface)
    resetDetail();
    resetResultat();
    // Affichage des bornes inférieures
    afficherResultat(i);
    // Remise à zéro des machines de l'instance
    i.resetMachines();
    // Affichage du résultat de l'algorithme LSA
    afficherResultatLSA(LSA(i));
    afficherDetail(i, "LSA");
    i.resetMachines();
    // Affichage du résultat de l'algorithme LPT
    afficherResultatLPT(LPT(i));
    afficherDetail(i, "LPT");
    i.resetMachines();
    // Affichage du résultat de l'algorithme MyAlgo
    afficherResultatMyAlgo(MyAlgo(i));
    afficherDetail(i, "MyAlgo");
    i.resetMachines();
}
