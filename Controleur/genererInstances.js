// retourne un nombre entier entre deux intervales min et max
function aleatoireInterval(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max+1);
    return Math.floor(Math.random() * (max - min)) + min;
}

// Génère une chaine de caractère "res" qui contient le résultat des algorithmes
// sur toutes les instances avec des ratios moyens
function genererResultat(instances, nom_fichier) {
    var res = ""; // Contenu du fichier
    var rLSA = 0, rLPT = 0, rMyAlgo = 0; // Résultats de chaque algorithme
    var rmLSA = 0, rmLPT = 0, rmMyAlgo = 0; // Ratios moyens
    var rLSAm = 0, rLPTm = 0, rMyAlgom = 0; // // Résultats de chaque algorithme
    // divisé par le maximum entre les bornes inférieures
    var m = 0; // Maximum entre les bornes inférieures
    var k = instances.length; // Nombre d'instances

    // Pour chaques instances
    for (let i = 0; i < k; i++) {
        // Calcul le maximum entre les bornes inférieures de l'instance courante
        m = Math.max(instances[i].borneInferieurMaximum, instances[i].borneInferieurMoyenne);

        // Inscrit les bornes inférieures de l'instance courante dans le fichier
        res += "Borne inférieur Maximum : " + instances[i].borneInferieurMaximum + "\r\n";
        res += "Borne inférieur Moyenne : " + instances[i].borneInferieurMoyenne + "\r\n";

        // Calcul le résultat LSA de l'instance courante
        rLSA = LSA(instances[i]);
        // Inscrit le résultat LSA de l'instance courante dans le fichier
        res += "Résultat LSA : " + rLSA + "\r\n";
        // Remet à zéro les machines de l'instance courante
        instances[i].resetMachines();

        // Même chose pour les deux autres algos
        rLPT = LPT(instances[i]);
        res += "Résultat LPT : " + rLPT + "\r\n";
        instances[i].resetMachines();
        rMyAlgo = MyAlgo(instances[i]);
        res += "Résultat MyAlgo : " + rMyAlgo + "\r\n";
        instances[i].resetMachines();

        res += "===\r\n"

        // Calcul les Ri (ratio des algorithmes) pour l'instance courante
        rLSAm = rLSA / m;
        rLPTm = rLPT / m;
        rMyAlgom = rMyAlgo / m;
        // Ajout du ratio pour faire la somme des ratios de toutes les instances (par algorithmes)
        rmLSA += rLSAm;
        rmLPT += rLPTm;
        rmMyAlgo += rMyAlgom;
    }
    // On divise les sommes des ratios par le nombre d'instances, ce qui donnes les ratios moyens
    rmLSA = rmLSA / k;
    rmLPT = rmLPT / k;
    rmMyAlgo = rmMyAlgo / k;

    // Inscrit les ratios moyens en fin de fichier
    res += "Ratio Moyen (LSA) : " + rmLSA + "\r\n";
    res += "Ratio Moyen (LPT) : " + rmLPT + "\r\n";
    res += "Ratio Moyen (MyAlgo) : " + rmMyAlgo + "\r\n";

    // Génère le fichier et l'envoi a l'utilisateur
    genererFichier(nom_fichier, res);
}
